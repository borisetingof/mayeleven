import { render, screen } from '@testing-library/react';

import App from './App';

test('renders progress bar', () => {
  render(<App />);
  const linkElement = screen.getByRole(/progressbar/i);
  expect(linkElement).toBeInTheDocument();
});
