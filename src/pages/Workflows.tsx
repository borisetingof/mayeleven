import { useQuery } from '@apollo/client';
import { Divider, LinearProgress } from '@material-ui/core';
import { useState } from 'react';

import Button from '../components/Button';
import { Container } from '../components/Layout';
import RadioOptionsInput from '../components/RadioOptionsInput';
import { Subtitle, Title } from '../components/Typography';
import { Workflows as WorkflowsType, Workflow as WorkflowType } from '../model';
import { WORKFLOWS } from '../queries';

const Workflows = () => {
  const { data, loading, error } =
    useQuery<{ Workflows: WorkflowsType }>(WORKFLOWS);

  const [workflow, setWorkflow] = useState<string>();

  if (loading) return <LinearProgress />;

  if (error) throw new Error();

  return (
    <>
      <Title>Start a workflow</Title>

      <Divider />

      <Container>
        <Subtitle>
          Answer few questionnaires and create a legally compliant to-do list
        </Subtitle>

        <RadioOptionsInput
          options={(data?.Workflows || []).map(
            ({ workflowId, workflowIcon, workflowName }: WorkflowType) => ({
              text: workflowName,
              icon: workflowIcon,
              iconPosition: '',
              value: workflowId,
            }),
          )}
          value={workflow}
          onChange={(value: string) => setWorkflow(value)}
        />

        <Button
          url={`/workflow/${
            data?.Workflows.find(
              ({ workflowName }) => workflowName === workflow,
            )?.workflowId
          }/step/1`}
          disabled={!workflow}>
          Next
        </Button>
      </Container>
    </>
  );
};

export default Workflows;
