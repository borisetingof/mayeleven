import { useQuery } from '@apollo/client';
import { ArrowLeftIcon } from '@heroicons/react/outline';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useContext } from 'react';
import { RouteComponentProps } from 'react-router-dom';

import Button from '../components/Button';
import Divider from '../components/Divider';
import { Container } from '../components/Layout';
import List from '../components/List';
import { Subtitle, Title } from '../components/Typography';
import { WorkflowContext } from '../Context';
import { Workflows as WorkflowsType } from '../model';
import { WORKFLOWS } from '../queries';

const Result: React.FC<
  RouteComponentProps<{ workflowId: string; index: string }>
> = ({
  history,
  match: {
    params: { workflowId },
  },
}) => {
  const { workflow } = useContext(WorkflowContext);

  const { data, loading, error } =
    useQuery<{ Workflows: WorkflowsType }>(WORKFLOWS);

  if (loading) return <LinearProgress />;

  if (error || !workflow?.[workflowId]) throw new Error();

  return (
    <>
      <Container row>
        <span onClick={() => history.goBack()} className="group cursor-pointer">
          <ArrowLeftIcon
            width="25"
            className="text-gray-900 group-hover:text-indigo-600"
          />
        </span>

        <Title>All done</Title>
      </Container>

      <Divider />

      <Container>
        <Subtitle>Confirm your selection</Subtitle>

        <List
          data={workflow?.[workflowId] || {}}
          title={
            data?.Workflows.find(({ workflowId: id }) => id === workflowId)
              ?.workflowName
          }
          subtitle="Workflow"
        />

        <Button url={`/`}>Restart</Button>
      </Container>
    </>
  );
};

export default Result;
