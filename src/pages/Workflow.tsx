import { useQuery } from '@apollo/client';
import { HomeIcon } from '@heroicons/react/outline';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useContext, useEffect } from 'react';
import { Link, Redirect, RouteComponentProps } from 'react-router-dom';

import Button from '../components/Button';
import Input from '../components/Input';
import { Container } from '../components/Layout';
import Progress from '../components/Progress';
import RadioOptionsInput from '../components/RadioOptionsInput';
import { Subtitle, Title } from '../components/Typography';
import { WorkflowContext } from '../Context';
import { getQuestionsFromWorkflow } from '../helpers';
import {
  ProgressStepVariant,
  WorkflowInterview,
  WorkflowState,
} from '../model';
import { WORKFLOW_INTERVIEW } from '../queries';

const Workflow: React.FC<
  RouteComponentProps<{ workflowId: string; index: string }>
> = ({
  match: {
    params: { workflowId, index },
  },
}) => {
  const { data, loading, error } = useQuery<{
    WorkflowInterview: WorkflowInterview;
  }>(WORKFLOW_INTERVIEW, {
    variables: { workflowId },
  });

  const stepIndex = parseInt(index, 10) - 1;
  const { workflow, setWorkflow } = useContext(WorkflowContext);

  useEffect(() => {
    if (stepIndex === 0) setWorkflow({});
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) return <LinearProgress />;

  if (error) throw new Error();

  if (stepIndex > 0 && !workflow[workflowId])
    return <Redirect to={`/workflow/${workflowId}/step/1`} />;

  const questions = getQuestionsFromWorkflow(data?.WorkflowInterview);
  const {
    question,
    input: { type, options },
  } = questions[stepIndex];

  const answer = workflow[workflowId]?.[question];
  const setAnswer = (value: string) =>
    setWorkflow((state: WorkflowState) => ({
      ...state,
      ...{
        [workflowId]: {
          ...(state?.[workflowId] || {}),
          [question]: value,
        },
      },
    }));

  return (
    <>
      <Container row>
        <Link to="/" className="group">
          <HomeIcon
            width="25"
            className="text-gray-900 group-hover:text-indigo-600"
          />
        </Link>

        <Title>{data?.WorkflowInterview.workflowName}</Title>
      </Container>

      <Progress
        steps={questions.map(({ question }, index) => {
          const answer = workflow[workflowId]?.[question];

          return {
            title: question,
            subtitle: answer,
            label: index + 1,
            variant:
              index === stepIndex
                ? ProgressStepVariant.Current
                : answer
                ? ProgressStepVariant.Checked
                : ProgressStepVariant.Normal,

            route: `/workflow/${workflowId}/step/${index + 1}`,
          };
        })}
        segments={3}
      />

      <Container>
        <Subtitle>{question}</Subtitle>

        {type === 'radioOptionsInput' ? (
          <RadioOptionsInput
            options={options}
            value={answer}
            onChange={setAnswer}
          />
        ) : (
          <Input
            value={answer}
            onChange={(event) => setAnswer(event.target.value)}
          />
        )}

        <Button
          url={
            stepIndex === questions.length - 1
              ? `/workflow/${workflowId}/result`
              : `/workflow/${workflowId}/step/${stepIndex + 2}`
          }
          disabled={!answer}>
          Next
        </Button>
      </Container>
    </>
  );
};

export default Workflow;
