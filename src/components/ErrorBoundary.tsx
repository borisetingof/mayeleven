import { EmojiSadIcon, HomeIcon } from '@heroicons/react/outline';
import React from 'react';

import Divider from '../components/Divider';
import { Container } from '../components/Layout';
import { Title } from '../components/Typography';

class ErrorBoundary extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = { error: null, errorInfo: null };
  }

  componentDidCatch(error: any, errorInfo: any) {
    this.setState({
      error: error,
      errorInfo: errorInfo,
    });
  }

  render() {
    return this.state.error ? (
      <>
        <Container row>
          <a href="/" className="group cursor-pointer">
            <HomeIcon
              width="25"
              className="text-gray-900 group-hover:text-indigo-600"
            />
          </a>

          <Title>Error</Title>
        </Container>

        <Divider />

        <Container>
          <EmojiSadIcon className="text-gray-900 group-hover:text-indigo-600" />
        </Container>
      </>
    ) : (
      this.props.children
    );
  }
}

export default ErrorBoundary;
