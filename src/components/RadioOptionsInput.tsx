import { RadioGroup } from '@headlessui/react';

import { classNames } from '../helpers';
import { QuestionInputOption } from '../model';

interface Props {
  options: QuestionInputOption[];
  value?: string;
  onChange: (value: string) => void;
  xl?: boolean;
}

const RadioOptionsInput: React.FC<Props> = ({
  options,
  value,
  onChange,
  xl,
}) => (
  <RadioGroup value={value} onChange={onChange}>
    <div className="rounded-md -space-y-px">
      {options.map(({ text, icon }, index) => (
        <RadioGroup.Option key={text} value={text}>
          {({ checked }) => (
            <div
              className={classNames(
                index === 0 ? 'rounded-tl-md rounded-tr-md' : '',
                index === options.length - 1
                  ? 'rounded-bl-md rounded-br-md'
                  : '',
                checked
                  ? 'bg-indigo-50 border-indigo-200 z-10'
                  : 'border-gray-200',
                xl ? 'p-8' : 'p-4',
                'relative border flex items-center cursor-pointer focus:outline-none',
              )}>
              <img src={icon} width={xl ? '40' : '30'} alt="icon" />

              <div
                className={classNames(xl ? 'ml-4' : 'ml-3', ' flex flex-col')}>
                <RadioGroup.Label
                  as="span"
                  className={classNames(
                    checked ? 'text-indigo-900' : 'text-gray-900',
                    xl ? 'text-lg font-bold' : 'text-sm font-medium',
                    'block',
                  )}>
                  {text}
                </RadioGroup.Label>
              </div>
            </div>
          )}
        </RadioGroup.Option>
      ))}
    </div>
  </RadioGroup>
);

export default RadioOptionsInput;
