export const Title: React.FC = ({ children }) => (
  <h1 className="p-8 text-2xl font-bold text-gray-900 text-center flex-grow">
    {children}
  </h1>
);

export const Subtitle: React.FC = ({ children }) => (
  <h2 className="p-8 text-2xl text-gray-900 text-center flex-grow">
    {children}
  </h2>
);
