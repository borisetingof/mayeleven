import React from 'react';

interface Props {
  value?: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input: React.FC<Props> = ({ value = '', onChange }) => (
  <input
    type="text"
    className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block  sm:text-sm border-gray-300 rounded-md w-full"
    value={value}
    onChange={onChange}
  />
);

export default Input;
