import { CheckIcon } from '@heroicons/react/solid';
import { Link } from 'react-router-dom';

import { classNames } from '../helpers';
import {
  ProgressStep as ProgressStepType,
  ProgressStepVariant,
} from '../model';

interface Props extends ProgressStepType {
  first: boolean;
  last: boolean;
  route: string;
}

const ProgressStep: React.FC<Props> = ({
  first,
  label,
  last,
  route,
  subtitle = '\u00A0',
  title,
  variant,
}) => (
  <li className="group relative overflow-hidden lg:flex-1">
    <div
      className={classNames(
        first ? 'border-b-0 rounded-t-md' : '',
        last ? 'border-t-0 rounded-b-md' : '',
        'border border-gray-200 overflow-hidden lg:border-0',
      )}>
      <span
        className={classNames(
          variant === ProgressStepVariant.Checked
            ? 'bg-transparent group-hover:bg-gray-200'
            : '',
          variant === ProgressStepVariant.Current ? 'bg-indigo-600' : '',
          'absolute top-0 left-0 w-1 h-full lg:w-full lg:h-1 lg:bottom-0 lg:top-auto',
        )}
        aria-hidden="true"
      />

      <span
        className={classNames(
          first ? '' : 'lg:pl-9',
          'px-6 py-5 flex items-start text-sm font-medium',
        )}>
        <span className="flex-shrink-0">
          <span
            className={classNames(
              variant === ProgressStepVariant.Checked ? 'bg-indigo-600' : '',
              variant === ProgressStepVariant.Current
                ? 'border-2 border-indigo-600'
                : '',
              variant === ProgressStepVariant.Normal
                ? 'border-2 border-gray-300'
                : '',
              'w-10 h-10 flex items-center justify-center rounded-full',
            )}>
            {variant === ProgressStepVariant.Checked ? (
              <CheckIcon className="w-6 h-6 text-white" aria-hidden="true" />
            ) : (
              <span
                className={
                  variant === ProgressStepVariant.Current
                    ? 'text-indigo-600'
                    : 'text-gray-500'
                }>
                {label}
              </span>
            )}
          </span>
        </span>

        <span className="mt-0.5 ml-4 min-w-0 flex flex-col">
          <span
            className={classNames(
              variant === ProgressStepVariant.Current ? 'text-indigo-600' : '',
              variant === ProgressStepVariant.Normal ? 'text-gray-500' : '',
              'text-xs font-semibold tracking-wide uppercase',
            )}>
            {title}
          </span>

          <span className="text-sm font-medium text-gray-500">{subtitle}</span>
        </span>
      </span>

      {variant === ProgressStepVariant.Checked ? (
        <Link to={route} className="absolute top-0 left-0 right-0 bottom-0" />
      ) : null}
    </div>

    {first ? null : (
      <div
        className="hidden absolute top-0 left-0 w-3 inset-0 lg:block"
        aria-hidden="true">
        <svg
          className="h-full w-full text-gray-300"
          viewBox="0 0 12 82"
          fill="none"
          preserveAspectRatio="none">
          <path
            d="M0.5 0V31L10.5 41L0.5 51V82"
            stroke="currentcolor"
            vectorEffect="non-scaling-stroke"
          />
        </svg>
      </div>
    )}
  </li>
);

export default ProgressStep;
