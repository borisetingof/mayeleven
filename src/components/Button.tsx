import { Link } from 'react-router-dom';

import { classNames } from '../helpers';

interface Props {
  url: string;
  disabled?: boolean;
}

const Button: React.FC<Props> = ({ children, url, disabled }) => (
  <div className="relative rounded-md shadow w-40 mx-auto my-4 sm:my-6 lg:my-8">
    <div
      className={classNames(
        disabled
          ? 'bg-white text-gray-300'
          : 'bg-indigo-600 hover:bg-indigo-700 text-white',
        'w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md md:py-4 md:text-lg md:px-10',
      )}>
      {children}
    </div>

    {disabled ? null : (
      <Link to={url} className="absolute top-0 left-0 right-0 bottom-0" />
    )}
  </div>
);

export default Button;
