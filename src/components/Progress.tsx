import {
  ProgressStep as ProgressStepType,
  ProgressStepVariant,
} from '../model';

import ProgressStep from './ProgressStep';

type StepWithRoute = ProgressStepType & {
  route: string;
};
interface Props {
  steps: StepWithRoute[];
  segments: number;
}

const Progress: React.FC<Props> = ({ steps, segments }) => {
  const current = steps.find(
    ({ variant }) => variant === ProgressStepVariant.Current,
  );

  const startIndex = Math.min(
    Math.max((current ? steps.indexOf(current) : 0) - 1, 0),
    steps.length - segments,
  );

  return (
    <div className="lg:border-t lg:border-b lg:border-gray-200">
      <nav className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <ol className="rounded-md overflow-hidden lg:flex lg:border-l lg:border-r lg:border-gray-200 lg:rounded-none">
          {steps.slice(startIndex, startIndex + segments).map((step, index) => {
            const props = {
              ...step,
              first: index === 0,
              last: index === segments - 1,
            };

            return <ProgressStep {...props} key={index} />;
          })}
        </ol>
      </nav>
    </div>
  );
};

export default Progress;
