import { classNames } from '../helpers';

interface Props {
  row?: boolean;
}

export const Container: React.FC<Props> = ({ children, row }) => (
  <div
    className={classNames(
      row ? 'flex flex-row items-center' : '',
      'container mx-auto max-w-screen-md px-4 sm:px-6 lg:px-8',
    )}>
    {children}
  </div>
);
