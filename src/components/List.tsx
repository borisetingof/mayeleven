import React from 'react';

interface Props {
  title?: string;
  subtitle?: string;
  data: { [key: string]: string | undefined };
}

const List: React.FC<Props> = ({ title, subtitle, data }) => (
  <div className="bg-white border border-gray-200 overflow-hidden rounded-lg">
    <div className="px-4 py-5 sm:px-6">
      <h3 className="text-lg leading-6 font-medium text-gray-900">{title}</h3>

      <p className="mt-1 max-w-2xl text-sm text-gray-500">{subtitle}</p>
    </div>

    <div className="border-t border-gray-200 px-4 py-5 sm:p-0">
      <dl className="sm:divide-y sm:divide-gray-200">
        {Object.entries(data).map(([key, value], index) => (
          <div
            className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6"
            key={index}>
            <dt className="text-sm font-medium text-gray-500 sm:col-span-2">
              {key}
            </dt>

            <dd className="mt-1 text-sm text-gray-900 sm:mt-0  sm:text-right">
              {value}
            </dd>
          </div>
        ))}
      </dl>
    </div>
  </div>
);

export default List;
