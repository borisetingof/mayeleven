import { gql } from '@apollo/client';

export const WORKFLOWS = gql`
  query Workflows {
    Workflows {
      workflowId
      workflowIcon
      workflowName
    }
  }
`;

export const WORKFLOW_INTERVIEW = gql`
  query WorkflowInterview($workflowId: String) {
    WorkflowInterview(workflowId: $workflowId) {
      workflowId
      workflowName
      workflowQuestions {
        questionPanels {
          panelId
          questions {
            question
            description
            input {
              type
              options {
                text
                icon
                value
              }
            }
          }
        }
      }
    }
  }
`;
