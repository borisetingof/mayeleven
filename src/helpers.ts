import { Question, WorkflowInterview } from './model';

export const getQuestionsFromWorkflow = (workflow?: WorkflowInterview) =>
  workflow?.workflowQuestions.questionPanels.reduce(
    (memo: Question[], panel) => [...memo, ...panel.questions],
    [],
  ) || [];

export const classNames = (...classes: any) =>
  classes.filter(Boolean).join(' ');
