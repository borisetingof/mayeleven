import React, { createContext, useState } from 'react';

import { WorkflowContext as WorkflowContextType, WorkflowState } from './model';

export const WorkflowContext = createContext<WorkflowContextType>({
  workflow: {},
  setWorkflow: () => null,
});

const WorkflowContextProvider: React.FC = (props) => {
  const [workflow, setWorkflow] = useState<WorkflowState>({});

  return (
    <WorkflowContext.Provider
      value={{
        workflow,
        setWorkflow,
      }}
      {...props}
    />
  );
};

export default WorkflowContextProvider;
