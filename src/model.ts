import { Dispatch, SetStateAction } from 'react';

export type QuestionInputOption = {
  text: string;
  icon: string;
  iconPosition: string;
  value: string;
};

export type QuestionInput = {
  type: string;
  options: QuestionInputOption[];
};

export type Question = {
  question: string;
  description: string;
  input: QuestionInput;
};

export type QuestionPanel = {
  panelId: string;
  questions: Question[];
};

export type WorkflowQuestions = {
  questionPanels: QuestionPanel[];
};

export type WorkflowInterview = {
  workflowId: string;
  workflowQuestions: WorkflowQuestions;
  workflowName: string;
};

export type Workflow = {
  workflowId: string;
  workflowIcon: string;
  workflowName: string;
};

export type Workflows = Workflow[];

export type WorkflowState = {
  [key: string]:
    | {
        [key: string]: string | undefined;
      }
    | undefined;
};

export type WorkflowContext = {
  workflow: WorkflowState;
  setWorkflow: Dispatch<SetStateAction<WorkflowState>>;
};

export enum ProgressStepVariant {
  Checked,
  Current,
  Normal,
}

export interface ProgressStep {
  label?: string | number;
  subtitle?: string;
  title: string;
  variant: ProgressStepVariant;
}
