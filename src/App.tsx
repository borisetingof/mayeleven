import {
  ApolloClient,
  ApolloLink,
  ApolloProvider,
  InMemoryCache,
  createHttpLink,
} from '@apollo/client';
import apolloLinkLogger from 'apollo-link-logger';
import { BrowserRouter, Route } from 'react-router-dom';

import ErrorBoundary from './components/ErrorBoundary';
import { uri } from './config.json';
import WorkflowContextProvider from './Context';
import Result from './pages/Result';
import Workflow from './pages/Workflow';
import Workflows from './pages/Workflows';

const App = () => (
  <ErrorBoundary>
    <ApolloProvider
      client={
        new ApolloClient({
          link: ApolloLink.from([apolloLinkLogger, createHttpLink({ uri })]),
          cache: new InMemoryCache(),
        })
      }>
      <WorkflowContextProvider>
        <BrowserRouter>
          <Route path="/" component={Workflows} exact />
          <Route
            path="/workflow/:workflowId/step/:index"
            component={Workflow}
          />
          <Route path="/workflow/:workflowId/result" component={Result} />
        </BrowserRouter>
      </WorkflowContextProvider>
    </ApolloProvider>
  </ErrorBoundary>
);

export default App;
